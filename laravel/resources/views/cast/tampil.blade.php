@extends('layout.master')
@section('title')
Data pemain baru
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary my-3">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
      <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $item->nama }}</td>
        <td>{{ $item->umur }}</td>
        <td>{{ $item->bio }}</td>
        <td>
        <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
        <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">edit</a>
        <form action="/cast/{{$item->id}}" method = "POST">
            @csrf
            @method('delete')
            <input type="submit" value="delete"class="btn btn-sm btn-danger">
        </form>
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5">No Users</td>
      </tr>
    @endforelse
  </tbody>
</table>

@endsection
