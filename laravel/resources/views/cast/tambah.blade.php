@extends('layout\master')
@section('title')
Data pemain baru
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="/cast" method="POST">
    @csrf

    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama">
    </div>

    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan umur">
    </div>

    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" id="bio" name="bio" rows="3" placeholder="Masukkan bio"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection