<?php
require 'animal.php';
require 'ape.php';
require 'frog.php';


$sheep = new Animal("shaun");
$kodok = new Frog("buduk");
$sungokong = new Ape("kera sakti");


echo "Name : {$sheep->name}<br>legs : {$sheep->legs}<br>cold blooded : {$sheep->cold_blooded}<br><br>";


echo "Name : {$kodok->name}<br>legs : {$kodok->legs}<br>cold blooded : {$kodok->cold_blooded}<br>";
$kodok->jump();
echo "<br><br>";


echo "Name : {$sungokong->name}<br>legs : {$sungokong->legs}<br>cold blooded : {$sungokong->cold_blooded}<br>";
$sungokong->yell();
echo "<br><br>";

?>